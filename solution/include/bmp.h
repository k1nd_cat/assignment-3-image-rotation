#ifndef LAB3_1_BMP_H
#define LAB3_1_BMP_H

#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

struct bmp_header init_header(uint64_t width, uint64_t height, long padding);

enum read_status from_bmp(FILE *fileName, struct image *image);

enum write_status to_bmp(FILE *fileName, struct image *image);

bool emptyImage(struct image *image, uint64_t width, uint64_t height);

const char* readStatus(enum read_status status);

const char* writeStatus(enum write_status status);

#endif

#ifndef LAB3_ROTATE_H
#define LAB3_ROTATE_H

#include "../include/bmp.h"
#include <malloc.h>
#include <stdint.h>

struct image rotate(struct image originImage, int angle);

#endif

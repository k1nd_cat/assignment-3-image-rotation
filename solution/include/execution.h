#ifndef LAB3_1_EXECUTION_H
#define LAB3_1_EXECUTION_H

#include "../include/exit_logic.h"
#include "bmp.h"
#include "rotation.h"
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>

bool execute(char *inputFile, char* angle, char* outputFile);

#endif

#ifndef LAB3_1_EXIT_LOGIC_H
#define LAB3_1_EXIT_LOGIC_H

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

void success_exit(void);
void error_exit(char* message);

#endif

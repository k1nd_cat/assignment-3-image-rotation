#include "../include/execution.h"
#include "../include/exit_logic.h"

int main(int argc, char** argv) {
    if (argc != 4) {
        error_exit("Invalid arguments");
    }

    execute(argv[1], argv[3], argv[2]);
    success_exit();
}

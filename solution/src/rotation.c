#include "../include/rotation.h"
#include <string.h>

struct image rotate(const struct image originImage, int angle) {
    struct image rotatedImage;
    int turnsCount = (angle / 90) % 4;

    if (turnsCount == 0) {
        rotatedImage.width = originImage.width;
        rotatedImage.height = originImage.height;
        rotatedImage.data = malloc(rotatedImage.width * rotatedImage.height * sizeof(struct pixel));
        memcpy(rotatedImage.data, originImage.data, rotatedImage.width * rotatedImage.height * sizeof(struct pixel));
        return rotatedImage;
    }

    if (turnsCount == 1 || turnsCount == 3) {
        rotatedImage.width = originImage.height;
        rotatedImage.height = originImage.width;
    } else {
        rotatedImage.width = originImage.width;
        rotatedImage.height = originImage.height;
    }

    rotatedImage.data = malloc(rotatedImage.width * rotatedImage.height * sizeof(struct pixel));

    for (uint64_t y = 0; y < originImage.height; y++) {
        for (uint64_t x = 0; x < originImage.width; x++) {
            uint64_t newX, newY;
            switch (turnsCount) {
                case 1:
                    newX = y;
                    newY = originImage.width - x - 1;
                    break;
                case 2:
                    newX = originImage.width - x - 1;
                    newY = originImage.height - y - 1;
                    break;
                case 3:
                    newX = originImage.height - y - 1;
                    newY = x;
                    break;
                default:
                    newX = x;
                    newY = y;
                    break;
            }
            rotatedImage.data[newY * rotatedImage.width + newX] = originImage.data[y * originImage.width + x];
        }
    }

    return rotatedImage;
}

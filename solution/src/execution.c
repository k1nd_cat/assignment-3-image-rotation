#include "../include/execution.h"

static bool initData(const char* fileName, struct image* image) {
    FILE* in = fopen(fileName, "rb");
    if (!in) return false;
    enum read_status status = from_bmp(in, image);
    fclose(in);
    readStatus(status);
    if (status != READ_OK) return false;

    return true;
}

static bool getAngle(char* angleStr, int* angle) {
    long int result = strtol(angleStr, NULL, 10);
    bool flag = false;
    if (result % 90 == 0 && result >= -270 && result <= 270) {
        flag = true;
    }

    if (!flag) return flag;

    if (result <= 0) result += 360;
    *angle = (int) result;
    return flag;
}

static bool writeData(struct image image, const char* fileName) {
    FILE* out = fopen(fileName, "wb");
    if (!out) return false;
    enum write_status status = to_bmp(out, &image);
    fclose(out);
    writeStatus(status);
    if (status != WRITE_OK) return false;

    return true;
}

static void freeData(struct image* image) {
    free(image->data);
}

bool execute(char* inputFileName, char* angleStr, char* outputFileName) {
    struct image initialImage = {0};
    struct image finalImage = {0};
    int angle = 0;
    if (!initData(inputFileName, &initialImage)) error_exit("Invalid input file");
    if (!getAngle(angleStr, &angle)) error_exit("Invalid angle");
    finalImage = rotate(initialImage, angle);
    if (!writeData(finalImage, outputFileName)) error_exit("Invalid write in file");
    freeData(&initialImage);
    freeData(&finalImage);

    return true;
}

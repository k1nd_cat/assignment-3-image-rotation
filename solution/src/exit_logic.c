#include "../include/exit_logic.h"

void success_exit(void) {
    printf("Successful completion of work");
    exit(0);
}

void error_exit(char* message) {
    errno = 43;
    perror(message);
    exit(43);
}

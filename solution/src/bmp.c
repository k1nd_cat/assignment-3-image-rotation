#include "../include/bmp.h"

static long calcPadding(uint32_t width) {
    return (4 - ((long) (width * sizeof(struct pixel)) % 4)) % 4;
}

struct bmp_header init_header(uint64_t width, uint64_t height, long padding) {
    struct bmp_header header = {0};

    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel) * (width + padding) * height;
    header.bfReserved = 0;
    header.bOffBits = 54;
    header.biSize = 40;
    header.biWidth = (uint32_t) width;
    header.biHeight = (uint32_t) height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = sizeof(struct pixel) * (width + padding) * height;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

enum read_status from_bmp(FILE *fileName, struct image *image) {
    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, fileName) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != 0x4D42 || header.bOffBits != 54) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != 24 || fseek(fileName, (long) header.bOffBits, SEEK_SET) != 0) {
        return READ_INVALID_HEADER;
    }

    if (!emptyImage(image, header.biWidth, header.biHeight)) {
        return READ_INVALID_BITS;
    }

    for (size_t i = 0; i < header.biHeight; i++) {
        if (fread((image->data) + i * header.biWidth, sizeof(struct pixel), header.biWidth, fileName) != header.biWidth) {
            return READ_INVALID_BITS;
        }

        if (fseek(fileName, calcPadding(header.biWidth), SEEK_CUR) != 0) {
            return READ_ERROR;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *fileName, struct image *image) {
    uint64_t width = image->width;
    uint64_t height = image->height;
    long padding = calcPadding(width);
    struct bmp_header header = init_header(width, height, padding);
    if (fileName == NULL) return WRITE_ERROR;
    fwrite(&header, sizeof (struct bmp_header), 1, fileName);
    for (size_t i = 0; i < height; i++) {
        if (fwrite(image->data + i * width, sizeof(struct pixel), width, fileName) != image->width) return WRITE_ERROR;
        if (fseek(fileName, padding, SEEK_CUR)) return WRITE_ERROR;
    }

    return WRITE_OK;
}

bool emptyImage(struct image *image, uint64_t width, uint64_t height) {
    if (!image) return false;
    image->height = height;
    image->width = width;
    image->data = malloc(sizeof(struct pixel) * width * height);
    if (!image->data) return false;

    return true;
}

const char* readStatus(enum read_status status) {
    if (status == READ_OK)
        return "Read file is ok";
    else if (status == READ_INVALID_SIGNATURE)
        return "Read invalid signature";
    else if (status == READ_INVALID_BITS)
        return "Read invalid bits";
    else if (status == READ_INVALID_HEADER)
        return "Read invalid header";
    else if (status == READ_ERROR)
        return "Read error";
    else
        return "";
}

const char* writeStatus(enum write_status status) {
    if (status == WRITE_OK)
        return "Write is ok";
    else if (status == WRITE_ERROR)
        return "Write error";
    else
        return "";
}
